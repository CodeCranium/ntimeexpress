﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nTimeExpress
{
    class ClockIn : Action
    {
        nTimeEntities nTimeDB = new nTimeEntities();

        public void Process()
        {
            var currentEmployee = EmployeeActions.GetEmployee();

            OperationLog logIn = new OperationLog()
                                     {
                                         EmployeeID = currentEmployee.EmpID,
                                         OperationType = Menu.Opts.Clock_In.ToString(),
                                         OperationDate = DateTime.Now
                                     };
            nTimeDB.OperationLogs.Add(logIn);

           ChangeEmployeeStatus updateEmployee = new ChangeEmployeeStatus();
           updateEmployee.ChangeStatus(currentEmployee, Menu.Opts.Clock_In);

            nTimeDB.SaveChanges();
        }

        
    }
}
