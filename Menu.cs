﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nTimeExpress
{
    public static class Menu
    {
        public enum Opts
        {
            Clock_In = 1,
            Clock_Out = 2,
            Get_list_of_Clocked_In_Employees = 3,
            Get_list_of_Clocked_Out_Employees = 4,
            Add_New_Employee = 5,
            Get_Employee_Total_Hours = 6,
        }
        public static Opts ShowMenu()
        {
            try
            {
                foreach (var menuOption in GetOptions())
                {
                    Console.WriteLine(menuOption);
                }
                var convertToOpt = Enum.Parse(typeof (Opts), value: Console.ReadLine());
                return (Opts) convertToOpt;
            }
            catch (ArgumentException invalidDataSpecified)
            {
                Console.WriteLine(invalidDataSpecified.Message);
                Console.ReadKey();
                throw;
            }
        }
       
        public static IEnumerable<object> GetOptions()
        {
            foreach (var opt in Enum.GetValues(typeof(Opts)))
            {
                if ((int)opt >= 1) 
                {
                    
                    yield return (int)opt + ": " + opt.ToString().Replace('_', ' ');
                }
                
            }
        }


    }
}
