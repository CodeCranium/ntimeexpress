﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace nTimeExpress
{
    class Program
    {
        
        static void Main(string[] args)
        {
            try
            {

                var x = Menu.ShowMenu();

                Type actionType = null;
                switch (x)
                {
                    case Menu.Opts.Clock_In:
                        actionType = typeof(ClockIn);
                        break;

                     case Menu.Opts.Clock_Out:
                        actionType = typeof (ClockOut);
                        break;

                    case Menu.Opts.Get_list_of_Clocked_In_Employees:
                        EmployeeActions.ShowClockedIn();
                        break;

                    case Menu.Opts.Get_list_of_Clocked_Out_Employees:
                        EmployeeActions.ShowClockedOut();
                        break;

                    case Menu.Opts.Add_New_Employee:
                        EmployeeActions.AddEmployee();
                        break;

                    case Menu.Opts.Get_Employee_Total_Hours:
                        EmployeeActions.GetEmployeeHours();
                        break;
                }

                var processMenuAction = (Action) Activator.CreateInstance(actionType);
                processMenuAction.Process();

            }
            catch (Exception)
            {
                Debugger.Break();  // Break for Debugging (If error occurs)
                Console.Clear();
                Main(null);
            }
            
        }

        
    }
}
