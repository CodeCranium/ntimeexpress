﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nTimeExpress
{
    class ClockOut : Action
    {
        nTimeEntities nTimeDB = new nTimeEntities();

        public void Process()
        {
            var currentEmployee = EmployeeActions.GetEmployee();

            OperationLog logOut = new OperationLog()
            {
                EmployeeID = currentEmployee.EmpID,
                OperationType = Menu.Opts.Clock_Out.ToString(),
                OperationDate = DateTime.Now
            };

            nTimeDB.OperationLogs.Add(logOut);

            ChangeEmployeeStatus updateEmployee = new ChangeEmployeeStatus();
            updateEmployee.ChangeStatus(currentEmployee, Menu.Opts.Clock_Out);

            nTimeDB.SaveChanges();
        }
    }
}
