﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nTimeExpress
{
    public static class EmployeeActions
    {
        static nTimeEntities nTimeDB = new nTimeEntities();

        public static Employee GetEmployee()
        {
            Console.Write("Employee ID? ");
            var inputEmpId = int.Parse(s: Console.ReadLine());
            return nTimeDB.Employees.FirstOrDefault(x => x.EmpID == inputEmpId);
        }

        public static IQueryable<Employee> GetClockedInEmployees(bool loggedIn = true)
        {
            var clockedInEmployees = from x in nTimeDB.Employees
                    where x.Status == loggedIn
                    select x;

            return clockedInEmployees;
        }

        public static void ShowClockedIn()
        {
            Console.Clear();
            foreach (var clockedIn in GetClockedInEmployees(true))
            {
                Console.WriteLine(String.Format("{0}: {1}, {2}", clockedIn.EmpID, clockedIn.LastName, clockedIn.FirstName));
            }
            Console.ReadKey();

        }

        public static void ShowClockedOut()
        {
            Console.Clear();
            foreach (var clockedOut in GetClockedInEmployees(false))
            {
                Console.WriteLine(String.Format("{0}: {1}, {2}", clockedOut.EmpID, clockedOut.LastName, clockedOut.FirstName));
            }
            Console.ReadKey();
        }

        public static void AddEmployee()
        {
            Console.Write("First Name? ");
            var fName = Console.ReadLine();

            Console.Write("Last Name? ");
            var lName = Console.ReadLine();
            var newEmp = new Employee()
                             {
                                 FirstName = fName,
                                 LastName = lName
                             };
            nTimeDB.Employees.Add(newEmp);

            nTimeDB.SaveChanges();
        }

        public static void GetEmployeeHours()
        {
            Console.Write("Employee ID? ");
            int empId = int.Parse(Console.ReadLine());

            IQueryable<OperationLog> empHours = nTimeDB.OperationLogs.Where(x => x.EmployeeID == empId);

            IList<ClockInOutActions> clockReport = new List<ClockInOutActions>();

            int itemCount = 0;

            foreach (var empHr in empHours)
            {
                if (empHr.OperationType == Menu.Opts.Clock_In.ToString())
                {
                    clockReport.Add(new ClockInOutActions()
                                     {
                                         ClockInTime = empHr.OperationDate
                                     });
                }
                else
                {
                    clockReport[itemCount].ClockOutTime = empHr.OperationDate;
                    itemCount += 1;
                }

                
            }

            TimeSpan totalHours = new TimeSpan();
            // use clockReport data to calcuate hours
            foreach (var clockHours in clockReport)
            {
                totalHours = totalHours.Add(clockHours.ClockOutTime.Subtract(clockHours.ClockInTime));
            }
           
            Console.WriteLine(String.Format("{0} Hours Total", totalHours.Hours));
            Console.ReadKey();
        }
    }


}
