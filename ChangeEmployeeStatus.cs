﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace nTimeExpress
{
    class ChangeEmployeeStatus
    {
        nTimeEntities nTimeDB = new nTimeEntities();
        public void ChangeStatus(Employee currentEmployee, Menu.Opts actionType)
        {
            // Get the instance of the Employee, to change their Status [True = Clocked In, False = Clocked Out]
            var emp = nTimeDB.Employees.Find(currentEmployee.EmpID);

            switch (actionType)
            {
                  case Menu.Opts.Clock_In:
                    emp.Status = true;
                    break;
                case Menu.Opts.Clock_Out:
                    emp.Status = false;
                    break;
            }
            
            nTimeDB.SaveChanges();
        }
    }
}
